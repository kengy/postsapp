package com.example.postsapp.utils

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity

object Utils {
	fun isNetworkAvaliable(activity: AppCompatActivity):Boolean{
		val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		val networkInfo=connectivityManager.activeNetworkInfo
		return  networkInfo!=null && networkInfo.isConnected
	}
}