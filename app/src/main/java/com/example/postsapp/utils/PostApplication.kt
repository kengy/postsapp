package com.example.postsapp.utils

import android.app.Application
import com.example.postsapp.repository.Repository
import com.example.postsapp.repository.dao.PostRoomDatabase

class PostApplication: Application() {

	val dataBase  by lazy {PostRoomDatabase.getDatabase(this)  }
	val repository by lazy { Repository(dataBase.postDao()) }

}