package com.example.postsapp.repository.api

import com.example.postsapp.model.Post
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RetrofitService {
	@GET("/posts")
	suspend fun getAllPosts(): Response <List<Post>>

	companion object {
		var retrofitService: RetrofitService? = null
		open fun getInstance() : RetrofitService {
			if (retrofitService == null) {
				val retrofit = Retrofit.Builder()
					.baseUrl("https://jsonplaceholder.typicode.com")
					.addConverterFactory(GsonConverterFactory.create())
					.build()
				retrofitService = retrofit.create(RetrofitService::class.java)
			}
			return retrofitService!!
		}
	}
}