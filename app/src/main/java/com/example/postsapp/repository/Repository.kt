package com.example.postsapp.repository

import com.example.postsapp.model.Post
import com.example.postsapp.repository.api.RetrofitService
import com.example.postsapp.repository.dao.PostDao

class Repository(private val postDao: PostDao) {

	private val retorfitService = RetrofitService.getInstance()

	suspend fun makePostRequest() = retorfitService.getAllPosts()

	suspend fun insert(lstPost: List<Post>){
		postDao.insertPosts(lstPost)
	}

	suspend fun getAllPosts() = postDao.getAllPosts()

}