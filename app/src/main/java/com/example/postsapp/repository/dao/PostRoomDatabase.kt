package com.example.postsapp.repository.dao

import android.content.Context
import androidx.room.*
import com.example.postsapp.model.Post

@Database(entities = arrayOf(Post::class), version = 2, exportSchema = false)
abstract class PostRoomDatabase : RoomDatabase() {

	abstract fun postDao(): PostDao

	companion object {
		@Volatile
		private var INSTANCE: PostRoomDatabase? = null

		fun getDatabase(context: Context): PostRoomDatabase {
			return INSTANCE ?: synchronized(this) {
				val instance = Room.databaseBuilder(
					context.applicationContext,
					PostRoomDatabase::class.java,
					"post"
				).build()
				INSTANCE = instance
				instance
			}
		}
	}
}