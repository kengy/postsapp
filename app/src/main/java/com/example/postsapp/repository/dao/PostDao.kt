package com.example.postsapp.repository.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.postsapp.model.Post

@Dao
interface PostDao {
	@Query("SELECT * FROM POSTS ORDER BY ID ASC")
	suspend fun getAllPosts(): List<Post>
	@Insert(onConflict = OnConflictStrategy.IGNORE)
	suspend fun insertPosts(lstPost:List<Post>)
}