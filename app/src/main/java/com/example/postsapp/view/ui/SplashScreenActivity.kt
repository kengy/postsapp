package com.example.postsapp.view.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.postsapp.R
import com.example.postsapp.view.ui.post.PostActivity
import kotlinx.android.synthetic.main.activity_splash_screen_activiy.*

class SplashScreenActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_splash_screen_activiy)

		imgSplashScreen.alpha = 0f
		imgSplashScreen.animate().setDuration(1500).alpha(1f).withEndAction{
			val i = Intent(this, PostActivity::class.java)
			startActivity(i)
			overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
			finish()
		}
	}
}