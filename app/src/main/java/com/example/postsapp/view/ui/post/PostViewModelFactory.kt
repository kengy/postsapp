package com.example.postsapp.view.ui.post

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.postsapp.repository.Repository
import java.lang.IllegalArgumentException

class PostViewModelFactory (private val repository: Repository): ViewModelProvider.Factory {
	override fun <T : ViewModel?> create(modelClass: Class<T>): T {
		return if (modelClass.isAssignableFrom(PostViewModel::class.java))
			PostViewModel (repository) as T
		else
			throw IllegalArgumentException("UnknownViewModel")
	}
}