package com.example.postsapp.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.postsapp.R
import com.example.postsapp.model.Post
import kotlinx.android.synthetic.main.post_item.view.*

class PostAdapter(val lstPosts: List<Post>, val onClick1: ()-> Unit, val onClick2: () -> Unit) :
	RecyclerView.Adapter<PostAdapter.PostsVieweHolder>() {
	override fun onCreateViewHolder(
		parent: ViewGroup,
		viewType: Int
	): PostsVieweHolder {

		val root = LayoutInflater.from(parent.context).inflate(R.layout.post_item,parent,false)
		return PostsVieweHolder(root)
	}

	override fun onBindViewHolder(holder: PostsVieweHolder, position: Int) {
		holder.bind(lstPosts.get(position))
	}

	override fun getItemCount() = lstPosts.size

	inner class PostsVieweHolder(private val binding: View): RecyclerView.ViewHolder(binding){

		fun bind(post:Post){
			binding.txtIdCard.text = post.id.toString()
			binding.txtCardTitle.text = post.title
			binding.txtBodyCard.text = post.body

			binding.btnAction1.setOnClickListener{
				onClick1()
			}

			binding.btnAction2.setOnClickListener{
				onClick2()
			}
		}

	}
}