package com.example.postsapp.view.ui.post

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.postsapp.R
import com.example.postsapp.utils.PostApplication
import com.example.postsapp.utils.Utils
import com.example.postsapp.view.adapters.PostAdapter
import kotlinx.android.synthetic.main.activity_post.*

class PostActivity : AppCompatActivity() {

	private var viewManager = LinearLayoutManager(this)
	private lateinit var postsRecycle: RecyclerView
	private lateinit var searchBtn: Button
	private val postViewModel: PostViewModel by viewModels {
		PostViewModelFactory((application as PostApplication).repository)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_post)

		searchBtn = findViewById(R.id.btnSearch)
		postsRecycle = findViewById(R.id.rcPosts)
		searchBtn.setOnClickListener {
			progress.visibility = View.VISIBLE
			if (Utils.isNetworkAvaliable(this))
				postViewModel.getPosts()
			else {
				postViewModel.getPostsFromDatabase()
			}
		}
		initialiserAdapter()
		observePosts()

	}

	fun initialiserAdapter() {
		postsRecycle.layoutManager = viewManager
	}

	fun btnClicked1() {
		Toast.makeText(this, "Hello, it's me :D", Toast.LENGTH_SHORT).show()
	}

	fun btnClicked2() {
		Toast.makeText(this, "Hello, it's me again :)", Toast.LENGTH_SHORT).show()
	}

	fun observePosts() {
		postViewModel.lst.observe(this, {
			postsRecycle.adapter = PostAdapter(
				it,
				{ btnClicked1() },
				{ btnClicked2() }
			)
		})

		postViewModel.progress.observe(this,{
			progress.visibility = View.GONE
		})
	}
}