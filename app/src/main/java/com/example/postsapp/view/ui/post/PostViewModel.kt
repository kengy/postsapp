package com.example.postsapp.view.ui.post

import androidx.lifecycle.*
import com.example.postsapp.model.Post
import com.example.postsapp.repository.Repository
import kotlinx.coroutines.*

	class PostViewModel(
		private val repository: Repository
	) : ViewModel() {

		var lst = MutableLiveData<List<Post>>()
		var progress= MutableLiveData<Boolean>()
		var job: Job? = null
		var errorMessage = MutableLiveData<String>()
		var exceptionHandler = CoroutineExceptionHandler { _, throwable ->
			onError("Exception handled: ${throwable.localizedMessage}")
		}

		fun getPosts() {
			job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
				val response = repository.makePostRequest()
				withContext(Dispatchers.Main) {
					if (response.isSuccessful) {
						lst.postValue(response.body())
						lst.value?.let { insert(it) }
						progress.value=false
					} else {
						onError("Error: ${response.message()}")
						progress.value=false
					}
				}
			}
		}

		fun onError(message: String) {
			errorMessage.value = message
			progress.value=false
		}

		override fun onCleared() {
			super.onCleared()
			job?.cancel()
		}

		fun insert(lstPost: List<Post>) = viewModelScope.launch {
			repository.insert(lstPost)
		}
		fun getPostsFromDatabase() {
			viewModelScope.launch {
				lst.value=  repository.getAllPosts()
				progress.value=false

			}

		}
}